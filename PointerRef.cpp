// PointerRef.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

class PlayersLeaderboard 
{
private:
	float score;
	string playerName;

public:
	float getScore() {
		return score;
	}

	string getPlayerName() {
		return playerName;
	}

	void setScore(float score) {
		this->score = score;
	}

	void setPlayerName(string playerName) {
		this->playerName = playerName;
	}
};

void fillArray(PlayersLeaderboard* plArray, int numberOfPlayers){

	for (int i = 0; i < numberOfPlayers; i++)
	{
		string playerName;
		cout << "Insert player name - ";
		cin >> playerName;
		float score;
		cout << "Insert player score - ";
		cin >> score;
		PlayersLeaderboard pLeaderboard;
		pLeaderboard.setScore(score);
		pLeaderboard.setPlayerName(playerName);
		plArray[i] = pLeaderboard;
	}
 }

void sortArray(PlayersLeaderboard* plArray, int numberOfPlayers) {

	for (int i = 0; i < numberOfPlayers; i++)
	{
		for (int j = 0; j < numberOfPlayers - 1; j++)
		{
			if (plArray[j].getScore() < plArray[j + 1].getScore()) {
				PlayersLeaderboard pScoreTemp = plArray[j];
				plArray[j] = plArray[j + 1];
				plArray[j + 1] = pScoreTemp;
			}
		}

	}
}

int main()
{
	int numberOfPlayers;
	cout << "Insert number of players - ";
	cin >> numberOfPlayers;
	PlayersLeaderboard* plArray = new PlayersLeaderboard[numberOfPlayers];

	fillArray(plArray, numberOfPlayers);
	sortArray(plArray, numberOfPlayers);

	for (int i = 0; i < numberOfPlayers; i++)
	{
		cout << "Player name : " << plArray[i].getPlayerName() << " Score : " << plArray[i].getScore() << "\n";
	}

	delete[] plArray;
}


